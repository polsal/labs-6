﻿using Lab6.ControlPanel.Contract;
using PK.Container;
using System;
using System.Reflection;

namespace Lab6.Infrastructure
{
    public struct LabDescriptor
    {
        #region P1

        public static Func<IContainer> ContainerFactory = () => new Container.Implementation.container();

        public static Assembly ControlPanelSpec = Assembly.GetAssembly(typeof(IControlPanel));
        public static Assembly ControlPanelImpl = Assembly.GetAssembly(typeof(void));

        public static Assembly MainComponentSpec = Assembly.GetAssembly(typeof(void));
        public static Assembly MainComponentImpl = Assembly.GetAssembly(typeof(void));

        #endregion
    }
}